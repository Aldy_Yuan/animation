import 'package:flutter/material.dart';
import 'fadeInOut.dart';
import 'shapeShifting.dart';
import 'testFlare.dart';
import 'heroAnimation.dart';
import 'staggerAnimation.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: 'Animation', home: Animation());
  }
}

class Animation extends StatefulWidget {
  @override
  _AnimationState createState() => _AnimationState();
}

class _AnimationState extends State<Animation> {
  void _fadeIn() {
    Navigator.of(context)
        .push(MaterialPageRoute<void>(builder: (BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: Text('FadeInOut'),
        ),
        body: FadeInOut(),
      );
    }));
  }

  void _shapeShifting() {
    Navigator.of(context)
        .push(MaterialPageRoute<void>(builder: (BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: Text('ShapeShifting'),
        ),
        body: ShapeShifting(),
      );
    }));
  }

  void _testFlare() {
    Navigator.of(context)
        .push(MaterialPageRoute<void>(builder: (BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: Text('TestFlare'),
        ),
        body: TestFlare(),
      );
    }));
  }

  void _heroAnimation() {
    Navigator.of(context)
        .push(MaterialPageRoute<void>(builder: (BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: Text('HeroAnimation'),
        ),
        body: HeroDemo(),
      );
    }));
  }

  void _staggerAnim() {
    Navigator.of(context)
        .push(MaterialPageRoute<void>(builder: (BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: Text('StaggeredAnimation'),
        ),
        body: StaggerDemo(),
      );
    }));
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Animation'),
          backgroundColor: Colors.blueAccent[400],
        ),
        body: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: MaterialButton(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 46.0),
                      child: Text(
                        'FadeInOut',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    color: Color.fromRGBO(0, 170, 0, 1),
                    onPressed: _fadeIn,
                  ),
                  flex: 2,
                ),
                Expanded(
                  child: MaterialButton(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 46.0),
                      child: Text(
                        'ShapeShifting',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    color: Color.fromRGBO(200, 170, 0, 1),
                    onPressed: _shapeShifting,
                  ),
                  flex: 2,
                )
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: MaterialButton(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 46.0),
                      child: Text(
                        'TestFlare',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    color: Color.fromRGBO(100, 50, 255, 1),
                    onPressed: _testFlare,
                  ),
                  flex: 2,
                ),
                Expanded(
                  child: MaterialButton(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 46.0),
                      child: Text(
                        'HeroAnimation',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    color: Color.fromRGBO(255, 100, 100, 1),
                    onPressed: _heroAnimation,
                  ),
                  flex: 2,
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: MaterialButton(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 46.0),
                      child: Text(
                        'StaggerAnimation',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    color: Color.fromRGBO(155, 100, 100, 1),
                    onPressed: _staggerAnim,
                  ),
                  flex: 2,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
