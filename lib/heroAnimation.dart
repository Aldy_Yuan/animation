import 'package:flutter/material.dart';

class HeroDemo extends StatefulWidget {
  @override
  _HeroDemoState createState() => _HeroDemoState();
}

class _HeroDemoState extends State<HeroDemo> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute<void>(
            builder: (BuildContext context) {
              return ShowHero();
            },
          ),
        );
      },
      child: Hero(
        tag: 'hero-tag',
        child: Container(
          width: 100.0,
          height: 100.0,
          margin: EdgeInsets.all(16.0),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage('assets/images/AOT-2.jpg'),
            ),
          ),
        ),
        flightShuttleBuilder:
            (flightContext, animation, direction, fromContext, toContext) {
          if (direction == HeroFlightDirection.push) {
            return Container(
              color: Colors.black,
              child: Image.asset('assets/images/AOT-2.jpg'),
            );
          } else {
            return Container(
              child: Image.asset('assets/images/AOT-2.jpg'),
            );
          }
        },
      ),
    );
  }
}

class ShowHero extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Hero(
        tag: 'hero-tag',
        child: Image.asset('assets/images/AOT-2.jpg'),
      ),
    );
  }
}
