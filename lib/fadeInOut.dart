import 'package:flutter/material.dart';

const owl_url =
    'https://raw.githubusercontent.com/flutter/website/master/src/images/owl.jpg';

class FadeInOut extends StatefulWidget {
  @override
  _FadeInOutState createState() => _FadeInOutState();
}

class _FadeInOutState extends State<FadeInOut> {
  double opacity = 0.0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(24.0),
          child: MaterialButton(
            color: Theme.of(context).primaryColor,
            child:
                Text('Show Animation', style: TextStyle(color: Colors.white)),
            onPressed: () => setState(() {
              if (opacity == 0.0) {
                opacity = 1.0;
              } else {
                opacity = 0.0;
              }
            }),
          ),
        ),
        AnimatedOpacity(
          duration: Duration(milliseconds: 300),
          opacity: opacity,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(32.0),
                child: Container(
                  decoration: BoxDecoration(boxShadow: [
                    BoxShadow(
                        blurRadius: 10.0,
                        offset: Offset(6, 10),
                        spreadRadius: 0,
                        color: Color.fromRGBO(70, 70, 70, 0.5))
                  ], borderRadius: BorderRadius.circular(24.0)),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(24.0),
                      child: Image.asset('assets/images/AOT-2.jpg')),
                ),
              ),
              Text('Type: Owl'),
              Text('Age: 39'),
              Text('Parent: Tio'),
            ],
          ),
        ),
      ],
    );
  }
}
