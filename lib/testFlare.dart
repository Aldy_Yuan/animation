import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';

class TestFlare extends StatefulWidget {
  @override
  _TestFlareState createState() => _TestFlareState();
}

class _TestFlareState extends State<TestFlare> {
  String _anim = "none";

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: () => setState(() {
          if (_anim == "down") {
            _anim = "right";
          } else {
            _anim = "down";
          }
        }),
        child: FlareActor(
          "assets/animations/TestFlare.flr",
          animation: _anim,
        ),
      ),
    );
  }
}
